import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import HttpApi from 'i18next-http-backend';

import LanguageDetector from 'i18next-browser-languagedetector';

i18n
  .use(LanguageDetector)
  .use(HttpApi)
  .use(initReactI18next)
  .init({
    lng: 'en',

    fallbackLng: 'en',

    keySeparator: false,

    ns: ['common'],

    defaultNS: 'common',

    interpolation: {
      escapeValue: false,
    },
  });

export default i18n;
