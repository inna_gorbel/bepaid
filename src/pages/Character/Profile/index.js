import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import { getCharacter } from 'redux/characters/selectors';

import Error from 'components/Error';

import * as S from './styled';

const Profile = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation('common');
  const { id } = useParams();
  const { isLoading, error, data } = useSelector(getCharacter);

  useEffect(() => {
    dispatch({ type: 'CHARACTER_FETCH_REQUESTED', id });
  }, [dispatch, id]);

  if (isLoading) {
    return <p>{t('loading')}...</p>;
  }

  if (error) {
    return <Error>{error}</Error>;
  }

  const { name, image, status, gender, species, type, origin, location } = data;

  return (
    <S.Wrapper>
      <S.Image>
        <S.Photo alt={name} src={image} />
      </S.Image>
      <S.Card>
        <S.Header>
          <S.Name>{name}</S.Name>
          <S.Status isAlive={status === 'Alive'}>{status}</S.Status>
        </S.Header>
        <S.Information>
          <S.Row>
            <S.Cell>Species</S.Cell>
            <S.Cell>{species}</S.Cell>
          </S.Row>
          <S.Row>
            <S.Cell>Gender</S.Cell>
            <S.Cell>{gender}</S.Cell>
          </S.Row>
          {type && (
            <S.Row>
              <S.Cell>Type</S.Cell>
              <S.Cell>{type}</S.Cell>
            </S.Row>
          )}
          <S.Row>
            <S.Cell>Origin</S.Cell>
            <S.Cell>{origin?.name}</S.Cell>
          </S.Row>
          <S.Row>
            <S.Cell>Location</S.Cell>
            <S.Cell>{location?.name}</S.Cell>
          </S.Row>
        </S.Information>
      </S.Card>
    </S.Wrapper>
  );
};

export default Profile;
