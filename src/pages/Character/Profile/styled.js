import styled from 'styled-components';

export const Wrapper = styled.div`
  border: ${({ theme }) => `${theme.borderWidth} solid ${theme.primaryColor}`};
  border-radius: ${({ theme }) => theme.borderRadius};
  overflow: hidden;
  display: flex;
  justify-content: flex-start;
  align-items: stretch;

  @media (max-width: ${({ theme }) => theme.mediaXXS}) {
    flex-direction: column;
  }
`;

export const Image = styled.div`
  width: 40%;
  background-color: ${({ theme }) => theme.textLightColor};

  @media (max-width: ${({ theme }) => theme.mediaXXS}) {
    width: 100%;
  }
`;

export const Photo = styled.img`
  width: 100%;
  height: 100%;
  object-fit: cover;
`;

export const Card = styled.div`
  width: 60%;
  padding: ${({ theme }) => `${theme.gap2} ${theme.gap4}`};

  @media (max-width: ${({ theme }) => theme.mediaXXS}) {
    width: 100%;
  }
`;

export const Header = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-end;

  &:not(:last-child) {
    margin-bottom: ${({ theme }) => theme.gap5};
  }
`;

export const Name = styled.h2`
  flex-grow: 1;
  font-size: ${({ theme }) => theme.fontSizeH1};
  line-height: ${({ theme }) => theme.lineHeightH1};
  font-weight: ${({ theme }) => theme.fontWeightTitle};
`;

export const Status = styled.p`
  flex-shrink: 0;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  text-align: right;

  &::before {
    content: '';
    width: ${({ theme }) => theme.iconWidth};
    height: ${({ theme }) => theme.iconHeight};
    border-radius: 50%;
    background-color: ${({ theme, isAlive }) => (isAlive ? theme.aliveColor : theme.notAliveColor)};
    flex-shrink: 0;
    margin-right: ${({ theme }) => theme.gap2};
  }
`;

export const Information = styled.div`
  border: ${({ theme }) => `${theme.borderWidth} solid ${theme.primaryColor}`};
`;

export const Row = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: stretch;

  &:not(:last-child) {
    border-bottom: ${({ theme }) => `${theme.borderWidth} solid ${theme.primaryColor}`};
  }
`;

export const Cell = styled.div`
  width: 50%;
  padding: ${({ theme }) => theme.gap};

  &:not(:last-child) {
    border-right: ${({ theme }) => `${theme.borderWidth} solid ${theme.primaryColor}`};
  }
`;
