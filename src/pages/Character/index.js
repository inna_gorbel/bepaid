import React from 'react';
import { useTranslation } from 'react-i18next';

import Main from 'components/Main';
import Container from 'components/Container';
import ButtonView from 'components/ButtonView';
import Profile from './Profile';

const Character = () => {
  const { t } = useTranslation('common');

  return (
    <Main>
      <Container>
        <Profile />
        <ButtonView buttonType="route" to="/">
          {t('homeButton')}
        </ButtonView>
      </Container>
    </Main>
  );
};

export default Character;
