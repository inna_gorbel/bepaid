import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import { getCharactersList } from 'redux/characters/selectors';

import Main from 'components/Main';
import Container from 'components/Container';
import Error from 'components/Error';
import List from './List';

const Home = () => {
  const { t } = useTranslation('common');
  const dispatch = useDispatch();
  const { isLoading, error, data } = useSelector(getCharactersList);
  const { results } = data;

  useEffect(() => {
    dispatch({ type: 'LIST_FETCH_REQUESTED' });
  }, [dispatch]);

  if (isLoading && !results.length) {
    return <p>{t('loading')}...</p>;
  }

  if (error && !results.length) {
    return <Error>{error}</Error>;
  }

  return (
    <Main isFullPage>
      <Container isFullPage>{results.length ? <List /> : <p>{t('noResults')}</p>}</Container>
    </Main>
  );
};

export default Home;
