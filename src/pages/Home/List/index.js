import React, { useState, useCallback, useEffect, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import { getCharactersList } from 'redux/characters/selectors';

import Marker from './Marker';
import Item from './Item';

import * as S from './styled';

const List = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation('common');

  const [currentList, setCurrentList] = useState([]);
  const [cursor, setCursor] = useState(0);
  const { isLoading, error, data: newData } = useSelector(getCharactersList);
  const { results, info } = newData;
  const { next } = info;

  const addItems = useCallback(
    number => {
      if (next && number > cursor) {
        dispatch({ type: 'PAGE_FETCH_REQUESTED', next });
        setCursor(number);
      }
    },
    [cursor, dispatch, next],
  );

  useEffect(() => {
    setCurrentList(results);
  }, [results]);

  return (
    <S.Wrapper>
      <S.List>
        {currentList.map(item => (
          <Fragment key={item.id}>
            <Item data={item} />
            {item.id % 20 === 0 && item.id > cursor && <Marker number={item.id} onShow={addItems} />}
          </Fragment>
        ))}
        {isLoading && <p>{t('loading')}...</p>}
        {error}
      </S.List>
    </S.Wrapper>
  );
};

export default List;
