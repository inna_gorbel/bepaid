import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';

import * as S from './styled';

const Item = ({ number, onShow }) => {
  const markerRef = useRef(null);

  useEffect(() => {
    const listObserver = new IntersectionObserver(entries => {
      entries.forEach(entry => {
        if (entry.isIntersecting) {
          onShow(number, markerRef);
        }
      });
    });
    listObserver.observe(markerRef.current);
  }, [onShow, number]);

  return <S.Marker ref={markerRef}>{number}</S.Marker>;
};

Item.propTypes = {
  number: PropTypes.number.isRequired,
  onShow: PropTypes.func.isRequired,
};

export default Item;
