import styled from 'styled-components';

export const Marker = styled.li`
  display: block;
  width: 0;
  height: 0;
  opacity: 0;
  font-size: 0;
  line-height: 0;
`;
