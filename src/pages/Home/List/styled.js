import styled from 'styled-components';

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;
  scrollbar-width: thin;
  scrollbar-color: ${({ theme }) => `${theme.scrollThumbColor} ${theme.scrollColor}`};

  &::-webkit-scrollbar {
    width: ${({ theme }) => theme.scrollWidth};
    background-color: ${({ theme }) => theme.scrollColor};
  }

  &::-webkit-scrollbar-thumb {
    height: ${({ theme }) => theme.scrollThumbHeight};
    background-color: ${({ theme }) => theme.scrollThumbColor};
  }

  &::-webkit-scrollbar-button:vertical:start:decrement {
    display: none;
  }

  &::-webkit-scrollbar-button:vertical:end:increment {
    display: none;
  }

  &::-webkit-scrollbar-button:horizontal:start:decrement {
    display: none;
  }

  &::-webkit-scrollbar-button:horizontal:end:increment {
    display: none;
  }
`;

export const List = styled.ul`
  display: block;
  width: 100%;
`;
