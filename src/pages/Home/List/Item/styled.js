import styled from 'styled-components';

export const Item = styled.li`
  &:not(:last-child) {
    margin-bottom: ${({ theme }) => theme.gapHalf};
  }
`;
