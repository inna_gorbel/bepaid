import React from 'react';
import PropTypes from 'prop-types';

import LinkView from 'components/LinkView';

import * as S from './styled';

const Item = ({ data }) => {
  const { id, name } = data;
  return (
    <S.Item>
      {id}. <LinkView to={`/character/${id}`}>{name}</LinkView>
    </S.Item>
  );
};

Item.propTypes = {
  data: PropTypes.shape().isRequired,
};

export default Item;
