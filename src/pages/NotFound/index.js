import React from 'react';
import { useTranslation } from 'react-i18next';

import Container from 'components/Container';
import Main from 'components/Main';
import Error from 'components/Error';
import ButtonView from 'components/ButtonView';

const NotFound = () => {
  const { t } = useTranslation('common');

  return (
    <Main isFullPage>
      <Container isFullPage>
        <Error>404</Error>
        <ButtonView buttonType="route" to="/">
          {t('homeButton')}
        </ButtonView>
      </Container>
    </Main>
  );
};

export default NotFound;
