import { http } from '../index';

const charactersAddress = 'character';

export const getCharactersList = () => {
  return http
    .get(charactersAddress)
    .then(response => response.data)
    .catch(({ response }) => response.data);
};

export const getCharactersPage = ({ url }) => {
  return http
    .get(url)
    .then(response => response.data)
    .catch(({ response }) => response.data);
};

export const getCharacter = ({ id }) => {
  return http
    .get(`${charactersAddress}/${id}`)
    .then(response => response.data)
    .catch(({ response }) => response.data);
};
