import axios from 'axios';

const baseURL = 'https://rickandmortyapi.com/api/';

export const http = axios.create({
  baseURL,
});
