const colors = {
  white: '#ffffff',

  grayLight: '#f7f7f7',
  gray: '#979ea8',
  grayOpacity10: 'rgba(151, 158, 168, 0.1)',
  grayOpacity20: 'rgba(151, 158, 168, 0.2)',
  grayOpacity30: 'rgba(151, 158, 168, 0.3)',
  grayDarker: '#1d2026',
  grayDark: '#15181d',

  blue: '#3b587e',

  green: '#62cbc1',
  greenLight: '#6ddacf',

  red: '#ef7c87',

  orange: '#f1843e',
};

const fontWeight = {
  fonWeightMedium: 500,
  fontWeightBold: 700,
};

const typography = {
  fontFamilyBody: 'Roboto',
  fontSizeBody: '14px',
  lineHeightBody: '1.4285714',

  fontSizeH1: '1.7142857rem',

  lineHeightH1: '1.3333333',

  buttonFontSize: '1em',
  buttonLineHeight: '1',
};

const media = {
  unit: '4',

  mediaXXS: '600px',
};

export const commonConstants = {
  ...typography,
  ...media,

  // common
  fontWeightBody: fontWeight.fonWeightMedium,
  accentColor: colors.green,
  borderRadius: `${media.unit}px`,
  borderWidth: '1px',
  transitionDelay: '0.3s',
  transitionDelayDropdown: '0.2s',
  fontWeightTitle: fontWeight.fontWeightBold,
  errorColor: colors.red,
  textLightColor: colors.gray,
  headerBorderColor: colors.grayOpacity20,
  triangleIcon: `${media.unit}px ${media.unit - 1}px 0 ${media.unit - 1}px`,
  triangleIconMargin: '0 5px 0 9px',
  containerWidth: `${300 * media.unit}px`,
  aliveColor: colors.green,
  notAliveColor: colors.orange,

  // gap
  gapHalf: `${media.unit / 2}px`,
  gap: `${media.unit}px`,
  gap2: `${2 * media.unit}px`,
  gap3: `${3 * media.unit}px`,
  gap4: `${4 * media.unit}px`,
  gap5: `${5 * media.unit}px`,

  gapBorder8: `${8 * media.unit - 1}px`,

  // Logo
  logoHeight: `${10 * media.unit}px`,
  logoWidth: `${10 * media.unit}px`,

  // button
  buttonBorderColor: colors.green,
  buttonColor: colors.green,
  buttonTextColor: colors.white,
  buttonHoverBorderColor: colors.greenLight,
  buttonHoverColor: colors.greenLight,
  buttonFontWeight: fontWeight.fontWeightBold,

  // link
  linkColor: colors.green,
  linkHoverColor: colors.greenLight,
  linkDisableColor: colors.gray,
  linkFontWeight: fontWeight.fontWeightBold,

  // scroll
  scrollColor: colors.grayOpacity10,
  scrollThumbColor: colors.grayOpacity30,
  scrollWidth: `${media.unit}px`,
  scrollThumbHeight: `47.5%`,

  // icon
  iconWidth: `${4 * media.unit}px`,
  iconHeight: `${4 * media.unit}px`,

  // select
  dropdownButtonHover: colors.grayOpacity10,
  dropdownShadow: '0 3px 5px rgba(9, 30, 66, 0.2)',
  dropdownDefault: `${media.unit}px ${2 * media.unit}px`,
  dropdownItemPadding: '6px',
};

export default colors;
