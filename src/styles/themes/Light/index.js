import colors, { commonConstants } from '../constants';

const Light = {
  ...commonConstants,
  bodyColor: colors.grayLight,
  panel: colors.white,
  textColor: colors.blue,
  primaryColor: colors.blue,
};

export default Light;
