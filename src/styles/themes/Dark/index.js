import colors, { commonConstants } from '../constants';

const Dark = {
  ...commonConstants,
  bodyColor: colors.grayDark,
  panel: colors.grayDarker,
  textColor: colors.white,
  primaryColor: colors.gray,
};

export default Dark;
