import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  * {
    padding: 0;
    margin: 0;
    outline: none;

    &,
    &::before,
    &::after {
      box-sizing: border-box;
    }
  }

  body {
    display: flex;
    flex: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  a {
    text-decoration: none;
    cursor: pointer;
    background-color: transparent;

    &,
    &:hover {
      color: inherit;
    }
  }

  button {
    cursor: pointer;
    background-color: transparent;
    border: none;
  }

  p,
  h1,
  h2,
  h3,
  h4,
  h5,
  h6,
  pre {
    margin: 0;
  }

  ul,
  ol {
    margin: 0;
    list-style: none;
  }

  button,
  input,
  optgroup,
  select,
  textarea {
    font-family: inherit;
    font-size: inherit;
    line-height: inherit;
    font-weight: inherit;
    color: inherit;
    border-radius: 0;
  }

  input,
  textarea {
    appearance: none;
    background-color: transparent;
  }

  input[type='number'] {
    appearance: none;
  }

  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    appearance: none;
    margin: 0;
  }

  input::-webkit-input-placeholder {
    color: ${({ theme }) => theme.textLightColor};
  }

  input::-moz-placeholder {
    color: ${({ theme }) => theme.textLightColor};
  }

  input:-moz-placeholder {
    color: ${({ theme }) => theme.textLightColor};
  }

  input:-ms-input-placeholder {
    color: ${({ theme }) => theme.textLightColor};
  }

  input:-webkit-autofill {
    -webkit-text-fill-color: ${({ theme }) => theme.textColor} !important;
    color: ${({ theme }) => theme.textColor} !important;
    font-size: ${({ theme }) => theme.fontSizeBody} !important;
    line-height: ${({ theme }) => theme.lineHeightBody} !important;
    font-weight: ${({ theme }) => theme.fontWeightBody} !important;
    font-family: ${({ theme }) => theme.fontFamilyBody}, sans-serif !important;
    background-color: transparent !important;
  }

  img,
  svg {
    display: block;
    max-width: 100%;
  }

  svg {
    fill: none;
  }

  html,
  body {
    background: ${({ theme }) => theme.bodyColor};
    color: ${({ theme }) => theme.textColor};
    font-size: ${({ theme }) => theme.fontSizeBody};
    line-height: ${({ theme }) => theme.lineHeightBody};
    font-weight: ${({ theme }) => theme.fontWeightBody};
    font-family: ${({ theme }) => theme.fontFamilyBody}, sans-serif;
  }

  #root {
    display: flex;
    flex: 1;
    flex-direction: column;
    min-height: 100vh;
  }
`;

export default GlobalStyle;
