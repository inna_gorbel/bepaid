export const LOCALISATION = [
  {
    code: 'en',
    description: 'English',
  },
  {
    code: 'ru',
    description: 'Русский',
  },
];
