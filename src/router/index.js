import React, { Suspense, lazy } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Header from 'components/Header';
import Footer from 'components/Footer';

const Home = lazy(() => import('pages/Home'));
const Character = lazy(() => import('pages/Character'));
const NotFound = lazy(() => import('pages/NotFound'));

const AppRouter = () => {
  return (
    <Router>
      <Suspense fallback={<div>Loading...</div>}>
        <Header />
        <Switch>
          <Route exact path="/character/:id" component={Character} />
          <Route exact path="/" component={Home} />
          <Route component={NotFound} />
        </Switch>
        <Footer />
      </Suspense>
    </Router>
  );
};

export default AppRouter;
