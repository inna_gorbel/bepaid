import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './reducers';
import rootSaga from './sagas';

const configure = initialState => {
  const sagaMiddleware = createSagaMiddleware();

  const middlewares = [sagaMiddleware];

  const store = configureStore({
    reducer: rootReducer,
    middleware: [...getDefaultMiddleware({ thunk: false }), ...middlewares],
    devTools: true,
    ...(initialState ? { initialState } : {}),
  });

  sagaMiddleware.run(rootSaga);

  return store;
};
export default configure;
