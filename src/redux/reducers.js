import { combineReducers } from 'redux';

import characters from './characters';
import settings from './settings';

const rootReducer = combineReducers({
  characters,
  settings,
});

export default rootReducer;
