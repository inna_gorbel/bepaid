import { call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import * as http from 'http/charcters';
import {
  getCharactersListRequest,
  getCharactersListSuccess,
  getCharactersListFailure,
  getNextPageRequest,
  getNextPageSuccess,
  getNextPageFailure,
  getCharacterRequest,
  getCharacterSuccess,
  getCharacterFailure,
} from './index';

function* fetchCharactersList() {
  try {
    yield put(getCharactersListRequest());
    const data = yield call(() => http.getCharactersList());
    if (data.error) {
      yield put(getCharactersListFailure(data.error));
    } else {
      yield put(getCharactersListSuccess(data));
    }
  } catch (error) {
    yield put(getCharactersListFailure(error));
  }
}

export function* list() {
  yield takeEvery('LIST_FETCH_REQUESTED', fetchCharactersList);
}

function* fetchCharactersPage(action) {
  try {
    yield put(getNextPageRequest());
    const data = yield call(() => http.getCharactersPage({ url: action.next }));
    if (data.error) {
      yield put(getNextPageFailure(data.error));
    } else {
      yield put(getNextPageSuccess(data));
    }
  } catch (error) {
    yield put(getNextPageFailure(error));
  }
}

export function* page() {
  yield takeLatest('PAGE_FETCH_REQUESTED', fetchCharactersPage);
}

function* fetchCharacter(action) {
  try {
    yield put(getCharacterRequest());
    const data = yield call(() => http.getCharacter({ id: action.id }));
    if (data.error) {
      yield put(getCharacterFailure(data.error));
    } else {
      yield put(getCharacterSuccess(data));
    }
  } catch (error) {
    yield put(getCharacterFailure(error));
  }
}

export function* character() {
  yield takeLatest('CHARACTER_FETCH_REQUESTED', fetchCharacter);
}
