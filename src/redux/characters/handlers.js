const initialState = {
  data: {
    info: {},
    results: [],
  },
  isLoading: false,
  error: '',
  currentCharacter: {
    isLoading: false,
    error: '',
    data: {},
  },
};

export const getCharactersListRequest = state => ({
  ...state,
  isLoading: true,
  error: '',
});

export const getCharactersListSuccess = (state, { payload }) => ({
  ...state,
  data: payload,
  isLoading: false,
});

export const getCharactersListFailure = (state, { payload }) => ({
  ...state,
  isLoading: false,
  error: payload,
});

export const getNextPageRequest = state => ({
  ...state,
  isLoading: true,
  error: '',
});

export const getNextPageSuccess = (state, { payload }) => {
  const { info, results: newResults } = payload;
  const { data } = state;
  const { results } = data;
  return {
    ...state,
    data: {
      info,
      results: [...results, ...newResults],
    },
    isLoading: false,
  };
};

export const getNextPageFailure = (state, { payload }) => ({
  ...state,
  isLoading: false,
  error: payload,
});

export const getCharacterRequest = state => {
  const { currentCharacter } = state;
  return {
    ...state,
    currentCharacter: {
      ...currentCharacter,
      isLoading: true,
      error: '',
    },
  };
};

export const getCharacterSuccess = (state, { payload }) => {
  const { currentCharacter } = state;
  return {
    ...state,
    currentCharacter: {
      ...currentCharacter,
      isLoading: false,
      data: payload,
    },
  };
};

export const getCharacterFailure = (state, { payload }) => {
  const { currentCharacter } = state;
  return {
    ...state,
    currentCharacter: {
      ...currentCharacter,
      isLoading: false,
      error: payload,
    },
  };
};

export default initialState;
