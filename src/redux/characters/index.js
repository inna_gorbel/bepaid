import { createSlice } from '@reduxjs/toolkit';

import initialState, * as handlers from './handlers';

const characters = createSlice({
  name: 'characters',
  initialState,
  reducers: {
    getCharactersListRequest: handlers.getCharactersListRequest,
    getCharactersListSuccess: handlers.getCharactersListSuccess,
    getCharactersListFailure: handlers.getCharactersListFailure,
    getNextPageRequest: handlers.getNextPageRequest,
    getNextPageSuccess: handlers.getNextPageSuccess,
    getNextPageFailure: handlers.getNextPageFailure,
    getCharacterRequest: handlers.getCharacterRequest,
    getCharacterSuccess: handlers.getCharacterSuccess,
    getCharacterFailure: handlers.getCharacterFailure,
  },
});

export const { actions, reducer } = characters;

export const {
  getCharactersListRequest,
  getCharactersListSuccess,
  getCharactersListFailure,
  getCharacterRequest,
  getCharacterSuccess,
  getCharacterFailure,
  getNextPageRequest,
  getNextPageSuccess,
  getNextPageFailure,
} = actions;

export default reducer;
