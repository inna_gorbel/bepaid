import { createSlice } from '@reduxjs/toolkit';

import initialState, * as handlers from './handlers';

const settings = createSlice({
  name: 'settings',
  initialState,
  reducers: {
    setTheme: handlers.setTheme,
  },
});

export const { actions, reducer } = settings;

export const { setTheme } = actions;

export default reducer;
