import { THEMES } from 'constants/theme';

const { LIGHT } = THEMES;

const initialState = {
  theme: LIGHT,
};

export const setTheme = (state, { payload }) => ({
  ...state,
  theme: payload,
});

export default initialState;
