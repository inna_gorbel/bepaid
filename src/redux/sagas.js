import { all } from 'redux-saga/effects';

import { list, page, character } from './characters/sagas';

export default function* rootSaga() {
  yield all([list(), page(), character()]);
}
