import { useEffect } from 'react';
import PropTypes from 'prop-types';

const useClickOutside = ({ reference, clickFunction }) => {
  useEffect(() => {
    const handleClickOutside = event => {
      const { target, type } = event;
      if (type === 'blur' || (reference.current && document.contains(target) && !reference.current.contains(target))) {
        clickFunction();
      }
    };

    document.addEventListener('click', handleClickOutside);
    window.addEventListener('blur', handleClickOutside);
    return () => {
      document.removeEventListener('click', handleClickOutside);
      window.removeEventListener('blur', handleClickOutside);
    };
  }, [clickFunction, reference]);
};

useClickOutside.propTypes = {
  reference: PropTypes.element.isRequired,
  clickFunction: PropTypes.func.isRequired,
};

export default useClickOutside;
