import React from 'react';
import { useSelector } from 'react-redux';
import { ThemeProvider } from 'styled-components';

import { getTheme } from 'redux/settings/selectors';

import GlobalStyle from 'styles/GlobalStyle';
import Light from 'styles/themes/Light';
import Dark from 'styles/themes/Dark';

import { THEMES } from 'constants/theme';
import AppRouter from './router';

const { DARK } = THEMES;

const App = () => {
  const theme = useSelector(getTheme);

  return (
    <ThemeProvider theme={theme !== DARK ? Light : Dark}>
      <GlobalStyle />
      <AppRouter />
    </ThemeProvider>
  );
};

export default App;
