import styled from 'styled-components';

export const Wrapper = styled.div`
  display: block;
  position: ${({ hasWrapper }) => !hasWrapper && 'relative'};
`;
