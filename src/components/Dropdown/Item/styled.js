import styled from 'styled-components';

export const Item = styled.button`
  display: ${({ isActive }) => (isActive ? 'none' : 'block')};
  width: 100%;
  padding: ${({ theme }) => theme.dropdownItemPadding};
  background-color: transparent;
  cursor: ${({ isSelected, isDisabled }) => !isSelected && !isDisabled && 'pointer'};
  transition: background-color ${({ theme }) => theme.transitionDelay} ease;
  text-align: left;
  color: ${({ theme, isDisabled }) => isDisabled && theme.textLightColor};

  &:hover,
  &:active,
  &:focus {
    background-color: ${({ theme, isDisabled }) => !isDisabled && theme.dropdownButtonHover};
  }
`;
