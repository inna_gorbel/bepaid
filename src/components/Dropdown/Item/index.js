import React, { useContext, useCallback } from 'react';
import PropTypes from 'prop-types';

import Context from '../Context';

import * as S from './styled';

const Item = ({ children, onClick, ...props }) => {
  const { close } = useContext(Context);

  const onClickCallback = useCallback(
    e => {
      e.preventDefault();
      onClick(e);
      close(true);
    },
    [onClick, close],
  );

  return (
    <S.Item onClick={onClickCallback} {...props}>
      {children}
    </S.Item>
  );
};

Item.propTypes = {
  children: PropTypes.node,
  onClick: PropTypes.func,
};

Item.defaultProps = {
  children: null,
  onClick: () => null,
};

export default Item;
