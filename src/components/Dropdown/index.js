import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';

import useClickOutside from 'hooks/useClickOutside';

import Context from './Context';

import Button from './Button';
import Content from './Content';
import Item from './Item';

import * as S from './styled';

const Dropdown = ({ children, onChange, ...props }) => {
  const [isOpen, setIsOpen] = useState(false);

  const wrapperRef = useRef(null);

  const clickButtonCallback = () => {
    setIsOpen(!isOpen);
    onChange(!isOpen);
  };

  const closeCallback = isInside => {
    if (isOpen) {
      setIsOpen(false);
      onChange(false, isInside);
    }
  };

  const openCallback = () => {
    if (!isOpen) {
      setIsOpen(true);
      onChange(true);
    }
  };

  useClickOutside({
    reference: wrapperRef,
    clickFunction: closeCallback,
  });

  const value = { close: closeCallback, open: openCallback, change: clickButtonCallback, isOpen };

  return (
    <Context.Provider value={value}>
      <S.Wrapper ref={wrapperRef} open={isOpen} {...props}>
        {children}
      </S.Wrapper>
    </Context.Provider>
  );
};

Dropdown.propTypes = {
  children: PropTypes.node.isRequired,
  onChange: PropTypes.func,
};

Dropdown.defaultProps = {
  onChange: () => {},
};

Dropdown.Content = Content;
Dropdown.Button = Button;
Dropdown.Item = Item;

export default Dropdown;
