import React, { useContext } from 'react';
import PropTypes from 'prop-types';

import Context from '../Context';

import * as S from './styled';

const Content = ({ children, ...props }) => {
  const { isOpen } = useContext(Context);

  return (
    <S.Content isOpen={isOpen} {...props}>
      {children}
    </S.Content>
  );
};

Content.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Content;
