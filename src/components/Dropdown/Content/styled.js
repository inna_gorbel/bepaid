import styled from 'styled-components';

export const Content = styled.div`
  display: block;
  position: absolute;
  top: calc(100% + ${({ theme }) => theme.gap});
  left: 0;
  background-color: ${({ theme }) => theme.panel};
  box-shadow: ${({ theme }) => theme.dropdownShadow};
  border-radius: ${({ theme }) => theme.borderRadius};
  width: ${({ isFull }) => isFull && '100%'};
  max-width: ${({ maxWidth }) => maxWidth};
  overflow: hidden;
  transform: scale(1, ${({ isOpen }) => (isOpen ? '1' : '0')});
  transform-origin: center top;
  transition: transform ${({ theme }) => theme.transitionDelayDropdown} ease;
`;
