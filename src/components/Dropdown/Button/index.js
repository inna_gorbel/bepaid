import React, { useContext, useCallback } from 'react';
import PropTypes from 'prop-types';

import Context from '../Context';

import * as S from './styled';

const Button = ({ name, onClick, ...props }) => {
  const { change, isOpen } = useContext(Context);

  const onClickCallback = useCallback(
    e => {
      e.preventDefault();
      onClick(e);
      change();
    },
    [onClick, change],
  );

  return (
    <S.Button type="button" onClick={onClickCallback} isOpen={isOpen} {...props}>
      {name && <S.Name>{name}</S.Name>}
    </S.Button>
  );
};

Button.propTypes = {
  onClick: PropTypes.func,
  name: PropTypes.string,
};

Button.defaultProps = {
  onClick: () => null,
  name: '',
};

export default Button;
