import styled from 'styled-components';

export const Button = styled.button`
  padding: ${({ theme }) => theme.dropdownDefault};
  border-radius: ${({ theme }) => theme.borderRadius};
  background-color: ${({ theme, isOpen }) => (isOpen ? theme.dropdownButtonHover : 'transparent')};
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  text-align: left;
  transition: all ${({ theme }) => theme.transitionDelay} ease;

  &:hover,
  &:focus {
    background-color: ${({ theme }) => theme.dropdownButtonHover};
  }

  &::after {
    content: '';
    width: 0;
    height: 0;
    border-style: solid;
    border-width: ${({ theme }) => theme.triangleIcon};
    border-color: ${({ theme }) => theme.textColor} transparent transparent transparent;
    flex-shrink: 0;
    margin: ${({ theme }) => theme.triangleIconMargin};
    transform: scale(1, ${({ isOpen }) => (isOpen ? '-1' : '1')});
    transition: transform ${({ theme }) => theme.transitionDelayDropdown} ease;
  }
`;

export const Name = styled.span`
  flex-grow: 1;
`;
