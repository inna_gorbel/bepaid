import styled from 'styled-components';

export const Main = styled.main`
  width: 100%;
  padding: ${({ theme }) => theme.gap4} 0;
  height: ${({ isFullPage, overHeight }) => isFullPage && overHeight && `calc(100vh - ${overHeight}px)`};
  flex-grow: 1;
`;
