import React from 'react';
import PropTypes from 'prop-types';

import * as S from './styled';

const Main = ({ children, ...props }) => {
  const header = document.getElementById('header');
  const footer = document.getElementById('footer');
  const height = header?.offsetHeight + footer?.offsetHeight;

  return (
    <S.Main {...props} overHeight={height}>
      {children}
    </S.Main>
  );
};

Main.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Main;
