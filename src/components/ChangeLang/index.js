import React from 'react';
import { useTranslation } from 'react-i18next';

import Dropdown from 'components/Dropdown';

import { LOCALISATION } from 'constants/localisation';

const ChangeLang = () => {
  const { i18n } = useTranslation();
  const { language } = i18n;

  const changeLanguage = ({ target }) => {
    const { value } = target;
    if (language !== value) {
      i18n.changeLanguage(value);
    }
  };

  return (
    <Dropdown>
      <Dropdown.Button name={LOCALISATION.filter(({ code }) => code === language)[0].description} />
      <Dropdown.Content>
        <div>
          {LOCALISATION.map(({ code, description }) => (
            <Dropdown.Item key={code} value={code} onClick={changeLanguage} isActive={language === code}>
              {description}
            </Dropdown.Item>
          ))}
        </div>
      </Dropdown.Content>
    </Dropdown>
  );
};

export default ChangeLang;
