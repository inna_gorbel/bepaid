import styled from 'styled-components';

export const Button = styled.button`
  display: block;
  text-align: center;
  border-radius: ${({ theme }) => theme.borderRadius};
  transition: all ${({ theme }) => theme.transitionDelay} ease;
  font-weight: ${({ theme }) => theme.buttonFontWeight};
  padding: ${({ theme }) => `${theme.gap3} ${theme.gapBorder8}`};
  font-size: ${({ theme }) => theme.buttonFontSize};
  line-height: ${({ theme }) => theme.buttonLineHeight};
  border-width: ${({ theme }) => theme.borderWidth};
  border-style: solid;
  background-color: ${({ theme }) => theme.buttonColor};
  border-color: ${({ theme }) => theme.buttonBorderColor};
  color: ${({ theme }) => theme.buttonTextColor};
  margin: 0 auto;
  max-width: 200px;

  &:not(:last-child) {
    margin-bottom: ${({ mb, theme }) => mb || theme.gap5};
  }

  &:not(:first-child) {
    margin-top: ${({ mb, theme }) => mb || theme.gap5};
  }

  &:disabled {
    opacity: 0.4;
    cursor: default;
  }

  &:not(:disabled) {
    &:hover,
    &:active {
      background-color: ${({ theme }) => theme.buttonHoverColor};
      border-color: ${({ theme }) => theme.buttonHoverBorderColor};
      color: ${({ theme }) => theme.buttonTextColor};
    }
  }
`;
