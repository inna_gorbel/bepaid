import React from 'react';
import PropTypes from 'prop-types';

import Button from './Button';
import Link from './Link';
import Route from './Route';

const types = {
  link: Link,
  route: Route,
  button: Button,
};

const ButtonView = ({ children, buttonType, ...props }) => {
  const ButtonComponent = types[buttonType];
  return <ButtonComponent {...props}>{children}</ButtonComponent>;
};

ButtonView.propTypes = {
  children: PropTypes.node.isRequired,
  buttonType: PropTypes.oneOf(['link', 'route', 'button']),
};

ButtonView.defaultProps = {
  buttonType: 'button',
};

export default ButtonView;
