import React from 'react';

import * as S from '../styled';

const Button = props => <S.Button as="button" {...props} />;

export default Button;
