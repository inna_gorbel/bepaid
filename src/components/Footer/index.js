import React from 'react';
import { useTranslation } from 'react-i18next';

import Container from 'components/Container';
import LinkView from 'components/LinkView';

import * as S from './styled';

const Footer = () => {
  const { t } = useTranslation('common');

  return (
    <S.Footer id="footer">
      <Container>
        <S.Copy>
          {t('copy')}{' '}
          <LinkView linkType="link" href="https://www.linkedin.com/in/инна-горбель-328301164" target="_blank">
            {t('author')}
          </LinkView>
        </S.Copy>
      </Container>
    </S.Footer>
  );
};

export default Footer;
