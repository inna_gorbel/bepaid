import styled from 'styled-components';

export const Footer = styled.footer`
  width: 100%;
  padding: ${({ theme }) => theme.gap2} 0;
  border-top: ${({ theme }) => `${theme.borderWidth} solid ${theme.headerBorderColor}`};
`;

export const Copy = styled.p`
  text-align: center;
`;
