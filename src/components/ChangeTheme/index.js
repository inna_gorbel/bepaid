import React from 'react';

import Dropdown from 'components/Dropdown';

import { THEMES } from 'constants/theme';
import { useDispatch, useSelector } from 'react-redux';
import { getTheme } from 'redux/settings/selectors';
import { setTheme } from 'redux/settings';

const ChangeTheme = () => {
  const dispatch = useDispatch();
  const currentTheme = useSelector(getTheme);
  const themesArray = Object.values(THEMES);

  const changeTheme = ({ target }) => {
    const { value } = target;
    if (currentTheme !== value) {
      dispatch(setTheme(value));
    }
  };

  return (
    <Dropdown>
      <Dropdown.Button name={currentTheme} />
      <Dropdown.Content>
        <div>
          {themesArray.map(item => (
            <Dropdown.Item key={item} value={item} onClick={changeTheme} isActive={currentTheme === item}>
              {item}
            </Dropdown.Item>
          ))}
        </div>
      </Dropdown.Content>
    </Dropdown>
  );
};

export default ChangeTheme;
