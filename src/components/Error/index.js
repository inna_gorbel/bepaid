import React from 'react';
import PropTypes from 'prop-types';

import * as S from './styled';

const Error = ({ children, ...props }) => <S.Error {...props}>{children}</S.Error>;

Error.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Error;
