import styled from 'styled-components';

export const Error = styled.p`
  color: ${({ theme }) => theme.errorColor};
  text-align: center;
`;
