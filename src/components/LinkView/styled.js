import styled from 'styled-components';

export const LinkView = styled.a`
  display: inline-block;
  text-align: ${({ textAlign }) => textAlign || 'left'};
  color: ${({ theme }) => theme.linkColor};
  text-decoration: underline solid transparent;
  transition: all ${({ theme }) => theme.transitionDelay} ease;
  font-weight: ${({ theme }) => theme.linkFontWeight};

  &:hover {
    color: ${({ theme }) => theme.linkHoverColor};
    text-decoration: underline solid ${({ theme }) => theme.linkHoverColor};
  }

  &:disabled {
    color: ${({ theme }) => theme.linkDisableColor};
    text-decoration: underline solid transparent;
    cursor: default;
  }
`;
