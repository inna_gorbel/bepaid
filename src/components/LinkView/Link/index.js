import React from 'react';
import PropTypes from 'prop-types';

import * as S from '../styled';

const Link = ({ href, ...props }) => (
  <S.LinkView as="a" href={href} rel="noopener noreferrer" target="_blank" {...props} />
);

Link.propTypes = {
  href: PropTypes.string.isRequired,
};

export default Link;
