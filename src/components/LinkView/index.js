import React from 'react';
import PropTypes from 'prop-types';

import Link from './Link';
import Route from './Route';

const types = {
  link: Link,
  route: Route,
};

const LinkView = ({ children, linkType, ...props }) => {
  const LinkComponentInner = types[linkType];
  return <LinkComponentInner {...props}>{children}</LinkComponentInner>;
};

LinkView.propTypes = {
  children: PropTypes.node.isRequired,
  linkType: PropTypes.oneOf(['link', 'route']),
};

LinkView.defaultProps = {
  linkType: 'route',
};

export default LinkView;
