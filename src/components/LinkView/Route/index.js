import React from 'react';
import PropTypes from 'prop-types';
import { Link as RouterLink } from 'react-router-dom';

import * as S from '../styled';

const Route = ({ to, ...props }) => <S.LinkView as={RouterLink} to={to} {...props} />;

Route.propTypes = {
  to: PropTypes.string.isRequired,
};

export default Route;
