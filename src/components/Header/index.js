import React from 'react';

import Container from 'components/Container';
import ChangeLang from 'components/ChangeLang';
import ChangeTheme from 'components/ChangeTheme';

import * as S from './styled';

const Header = () => {
  return (
    <S.Header id="header">
      <Container>
        <S.Grid>
          <S.GridItem isLeft>
            <S.Logo to="/">
              <S.Icon />
            </S.Logo>
          </S.GridItem>
          <S.GridItem>
            <ChangeLang />
          </S.GridItem>
          <S.GridItem>
            <ChangeTheme />
          </S.GridItem>
        </S.Grid>
      </Container>
    </S.Header>
  );
};

export default Header;
