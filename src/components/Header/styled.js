import styled from 'styled-components';
import { Link as RouterLink } from 'react-router-dom';

import { ReactComponent as LogoImage } from './assets/logo.svg';

export const Header = styled.div`
  width: 100%;
  padding: ${({ theme }) => theme.gap4} 0;
  border-bottom: ${({ theme }) => `${theme.borderWidth} solid ${theme.headerBorderColor}`};
`;

export const Logo = styled(RouterLink)`
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const Icon = styled(LogoImage)`
  height: ${({ theme }) => theme.logoHeight};
  width: ${({ theme }) => theme.logoWidth};
  fill: ${({ theme }) => theme.textColor};
  transition: fill ${({ theme }) => theme.transitionDelay} ease;

  &:hover {
    fill: ${({ theme }) => theme.accentColor};
  }
`;

export const Grid = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin: 0 -${({ theme }) => theme.gap};
`;

export const GridItem = styled.div`
  padding: 0 ${({ theme }) => theme.gap};
  margin-right: ${({ isLeft }) => isLeft && 'auto'};
`;
