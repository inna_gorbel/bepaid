import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: ${({ isFullPage }) => isFullPage && '100%'};
  padding: 0 ${({ theme }) => theme.gap4};
  margin: 0 auto;
  max-width: ${({ theme }) => theme.containerWidth};
`;
